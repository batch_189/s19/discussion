// alert ('Hello!, Discussion 19');

/*
Selector 
    -sorts out whether the statements/s are to be executed based in the
    condition whether ti is true/false.

    if . . else statement

    Syntax:
        if(condition) {
            //statement
        } else {
            //statement
        }

    if statement - Executes condition if true
    syntax:
        if(condition) {
            //statement
        }
*/

let numA = -1

if(numA < 0) {
    console.log("Hello");
}
console.log(numA < 0);

let city = "New York"

if (city === "New York") {
    console.log("Welcome to new york city");
}

/* 
Else if 
    - execute a statement if previous conditions is are false and o=if the specified
    condition is true.
    -the else if clause is optional and can be added to capture additional conditions
    to change the flow of a program.
*/

let numB = 1;

if (numB > 0) {
    console.log('Hello');
} else if (numB >0 ){
    console.log('World');
}

/* else
    -execute a statement if all other conditions are false
*/

if (numA > 0) {
    console.log ("hello");
} else if (numB === 0 ) {
    console.log("world")
} else {
        console.log("again");
    }

// parseInt to convert into string
// let age = parseInt(prompt("Enter your age: "))

// if (age <= 18) {
//     console.log("Not allowed to drink!");
// }   else {
//     console.log("Too old ka na, Shot na!");
// }

/* Mini-Activity
    if height is below 150, display "Did not passed the minimum height requirement". 
    if above 150, display "Passed the minimum height requirement".
*/

// let height = parseInt(prompt("Enter your height"));

function getHeight (height){
    if (height <= 150) {
        console.log("Did not passed the minimum height requirement!");
    }
    else {
        console.log("Passed the minimum height Requirement!");
    }
}
getHeight(149);
getHeight(155);

// other solutions
let height = 160;

if (height <= 150) {
    console.log("Did not passed! Too short")
} else {
    console.log("Passed! Tall Enough")
}

// 
let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
    if (windSpeed < 61) {
        return "Not a typhoon yet"
    } else if ( windSpeed > 61) {
        return "Tropical depression detected"
    } else if ( windSpeed >= 62 && windSpeed >= 88) {
        return "Tropical storm detected"
    } else if (windSpeed >= 89 && windSpeed <= 117) {
        return "severe tropical storm detected"
    } else {
        return "typhoon detected"
    }
}
message = determineTyphoonIntensity(200);
console.log(message);
// or invoke this way
console.log(determineTyphoonIntensity(30));

if (message == "Tropical depression detected") {
    console.warn(message);
}

// Truthy and Falsey
/* In JS. a truthy value is a value that is considered 
true when encountered in a boolean context.*/

// Truthy Example
if (true) {
    console.log("Truthy");
}
if (1) {
    console.log("Truthy");
}
if ([]) {
    console.log("Truthy");
}

// Falsy Example
if (false) {
    console.log("Falsy");
}
if (0) {
    console.log("Falsy");
}
if (undefined) {
    console.log("Falsy");
}

// Conditional (Ternary) Operator
/*
        Ternary Operator takes in there operands.
        1. Condition
        2. Expression to execute if the condition is truthy.
        3. Expression to execute if the condition is falsy.

    Syntax:
        let variable = (condition) ? ifTrue_expression : ifFalse_expression
*/
// Single statement Execution
// mostly use kapag hindi ganun kahaba ang result ng statement

let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is false";
console.log("Result of the Ternary Operator: " + ternaryResult); // true

// Multiple statement execution
let userName;

function isofLegalAge() {
    userName = 'chen'; 
    return 'You at legal age limit';
}

function isUnderAge() {
    userName = 'Poi';
    return 'You are under the age limit';
}
// prompt accepting string data  type that's why we used parseInt to convert into number data type.
let age = parseInt(prompt("What is your age"));
console.log(age);
let legalAge = (age > 18) ? isofLegalAge() : isUnderAge();
console.log("Result of ternary operator in function: " + legalAge + ',' + userName);

// SWITCH Statement
/*
    cane be used as an alternative to an if.. else statement where the data to be used
    in the condition is of an expected input.

    SYNTAX:
        switch (expression) {
            case <value>:
                statement;
                break;

            default: 
                statement;
                beak;
        }
    -default parang si else
*/

let day  = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
    case 'monday':
        console.log("The color of hte day is red.")
        break;
    case 'tuesday':
        console.log('the color of the day is orange');
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is blue");
        break;
    case 'friday':
        console.log("The color of the day is white");
        break;
    case 'saturday':
        console.log("The color of the day is green");
        break;
    case 'sunday':
        console.log("The color of the day is pink");
        break;
    default:
        console.log("please input a valid day.");
        break;         
}
// need break statement to stop the loop when found.

// TRY and CATCH
/*
    -try catch are commonly use for error handling
*/
// alerat is the error that found
function showIntensityAlert(windSpeed) {
    try {
        alerat(determineTyphoonIntensity(windSpeed))
    }
    catch (error){
        console.log(typeof error);
        console.warn(error.message);
    }
    finally {
        alert("Intensity update will show new alert!!");
    }
}
showIntensityAlert(56);